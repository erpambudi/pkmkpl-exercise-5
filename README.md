# PKMKPL Exercise 5 Input Validation dan Test Organization

Sumber latihan ini di dapatkan dari buku :

[Test-Driven Development with Python, by Harry Percival O Reilly, 2017](https://www.obeythetestinggoat.com/pages/book.html)

Project Sebelumnya dapat di akses di
[Exercise 4](https://gitlab.com/erpambudi/pkmkpl-exercise-4.git)

Project Django ini adalah sebuah halaman website sederhana dan masih berada di local server.
Untuk melihat halaman website yang sudah di deploy bisa di akses di :
https://erpamweb.herokuapp.com/


Jika website tidak bisa di buka, kemungkinan website dalam mode sleep. Cobalah untuk me refresh website secara berulang.



Di dalam Latihan Exercise 5 kali ini yaitu :
1.  Menerapkan Unit Testing Model Validation dan Melakukan Refactoring
2.  Membuat Functional Testing input validation


**Menerapkan Unit Testing Model Validation dan Melakukan Refactoring**

**1. Functional Tests**

Struktur Folder functional_tests sebelumnya :

```
.
├── functional_tests
│   ├── __pychache__
│   ├── __init__.py
│   └── tests.py
.
```


Jika sebelumnya file testing yang berada di dalam folder functional_tests yaitu file tests.py mempunyai satu class yaitu NewVisitorTest(StaticLiveServerTestCase) dan mempunyai beberapa method untuk jenis testing yang berbeda,
maka kali ini struktur dari file testing tersebut akan di ubah dan di kelompokan berdasarkan jenis testing nya. berikut ini merupakan struktur folder functional_tests yang baru.

Struktur Folder functional_tests yang baru :

```
.
├── functional_tests
│   ├── __pychache__
│   ├── __init__.py
│   ├── base.py
│   ├── test_layout_and_styling.py
│   ├── test_list_item_validation.py
│   └── test_simple_list_creation.py
.
```

Perubahan struktur file tersebut merupakan pengelompokan dari jenis testing atau method yang di gunakan untuk melakukan testing.
`base.py` merupakan file utama yang menyediakan method dasar untuk melakukan testing.
method - method yang ada di dalam `base.py` nantinya dapat di akses oleh file lain yang sudah melakukan `import base.py` .
kemudian `test_layout_and_styling.py` yang sebelumnya berada di satu file yang sama dan merupakan file yang berisi method untuk melakukan layout dan styling testing kini berdiri sendiri dan mempunyai
class sendiri yaitu `class LayoutAndStylingTest(FunctionalTest)` . Begitu juga dengan file `test_list_item_validation.py` yang sebelumnya berada pada satu file yang sama, sekarang memiliki file tersendiri
 dan mempunyai `class ItemValidationTest(FunctionalTest)` . file `test_simple_list_creation.py` juga sama memiliki class sendiri yaitu `class NewVisitorTest(FunctionalTest)` .
Dengan melakukan pengelompokan file testing seperti ini maka struktur project akan menjadi rapih dan mudah untuk melakukan perubahan pada code.


Menjalankan semua functional testing :

> **erpamDjango>python manage.py test functional_tests**



Menjalankan salah satu functional testing :

> **erpamDjango>python manage.py test functional_tests.test_list_item_validation**



**2. Unit Test Tests**

Struktur Folder lists sebelumnya :

```
.
├── lists
│   ├── __pychache__
│   ├── migrations
│   ├── templates
│   ├── __init__.py
│   ├── admin.py
│   ├── apps.py
│   ├── models.py
│   ├── tests.py
│   ├── views.py
│   └── static \ bootstrap
.
```


Struktur Folder lists yang baru :

```
.
├── lists
│   ├── __pychache__
│   ├── migrations
│   ├── templates
│   ├── tests
│   │   ├── __pychache__
│   │   ├── __init__.py
│   │   ├── test_models.py
│   │   └── test_views.py
│   ├── __init__.py
│   ├── admin.py
│   ├── apps.py
│   ├── models.py
│   ├── tests.py
│   ├── views.py
│   └── static \ bootstrap
.
```

Sama seperti functional test, Unit test juga di ubah dan di kelompokan berdasarkan jenis testingnya masing - masing, yaitu `test_models.py` dan `test_views.py`
Untuk menjalankan unit testing sebagai berikut :

> **erpamDjango>python manage.py test lists**


**Membuat Functional Testing input validation**

Pada latihan kali ini, di buat sebuah functional testing dimana inputan akan di cek apakah kosong atau tidak, jika inputan kosong maka akan terjadi error pada testing.
testing ini di lakukan dengan mengambil elemen css selector untuk mengecek apakah input box kosong atau tidak. testing ini juga menggunakan method baru yang ada di `base.py` yaitu `wait_for()`
dimana terdapat lambda untuk membuat fungsi satu baris atau fungsi sekali pakai. 
berikut ini method yang ada di file testing `test_list_item_validation.py`

```
def test_cannot_add_empty_list_items(self):

    self.browser.get(self.live_server_url)
        
    self.browser.find_element_by_id('id_new_item').send_keys('Buy milk')
    self.browser.find_element_by_id('id_new_item').send_keys(Keys.ENTER)
    self.wait_for_row_in_list_comment('1: Buy milk')

    self.browser.find_element_by_id('id_new_item').send_keys(Keys.ENTER)

    self.wait_for(lambda: self.assertEqual(
        self.browser.find_element_by_css_selector('.has-error').text,
        "You can't have an empty list"
    ))
       
    self.browser.find_element_by_id('id_new_item').send_keys('Make tea')
    self.browser.find_element_by_id('id_new_item').send_keys(Keys.ENTER)
    self.wait_for_row_in_list_comment('1: Buy milk')
    self.wait_for_row_in_list_comment('2: Make tea')

    self.fail('finish this test!')
```
