from selenium.webdriver.common.keys import Keys
from .base import FunctionalTest

class LayoutAndStylingTest(FunctionalTest):

    def test_layout_and_styling_table(self):
        
        self.browser.get(self.live_server_url)
        
        #inisialisasi id tabel to do list
        table = self.browser.find_element_by_id('id_todo_table')

        # melakukan pengecekan posisi tabel di tengah layout atau tidak
        self.assertAlmostEqual(
            table.location['x'] + table.size['width']/2,
            512,
            delta=10
        )

        #jika semuanya true maka test berhasil dan akan menampilkan pesan di CMD
        self.fail('OK Finish the test!')

    def test_layout_and_styling_comment(self):
        
        self.browser.get(self.live_server_url)
        
        #inisialisasi id tabel komentar
        table = self.browser.find_element_by_id('id_list_table')

        # melakukan pengecekan posisi tabel di tengah layout atau tidak
        self.assertAlmostEqual(
            table.location['x'] + table.size['width']/2,
            512,
            delta=10
        )

        #jika semuanya true maka test berhasil dan akan menampilkan pesan di CMD
        self.fail('OK Finish the test!')