from .base import FunctionalTest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

class NewVisitorTest(FunctionalTest):  

    def test_can_start_a_list_and_retrieve_it_later(self):  
        
        self.browser.get(self.live_server_url)
        
        # functional test, mencari elemen tag h1
        # kemudian mencari text To-Do di dalam element h1 tersebut
        header_text = self.browser.find_element_by_tag_name('h1').text  
        self.assertIn('To-Do', header_text)

        inputbox = self.browser.find_element_by_id('id_new_item')  
        self.assertEqual(
            inputbox.get_attribute('placeholder'),
            'Enter a to-do item'
        )
        
        #unit test, komentar otomatis jika ada item lebih dari 6 di To-Do list
        self.wait_for_row_in_list_table('6: Tugas Technopreneurship')

        inputbox.send_keys('Oh Tidaaaakk banyak sekali tugas kali ini... Bagaimana iniiiiiii')
        inputbox.send_keys(Keys.ENTER)

        #jika semuanya true maka test berhasil dan akan menampilkan pesan di CMD
        self.fail('OK Finish the test!')